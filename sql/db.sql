DROP DATABASE IF EXISTS `study_app`;
CREATE DATABASE `study_app`;

CREATE TABLE `users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` CHAR(255),
  `last_name` CHAR(255),
  `first_name` CHAR(255),
  `middle_name` CHAR(255),
  `password` CHAR(255),
  PRIMARY KEY (`id`)
);