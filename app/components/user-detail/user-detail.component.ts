import { Component, OnInit }                    from '@angular/core';
import { ActivatedRoute, Params, Router }       from '@angular/router';

import { User }                                 from "../../models/user";
import { UserService }                          from "../../services/user.service";

@Component({
    selector: 'user-detail',
    template: `
        <a (click)="gotoList()"><< Back to user list</a>
        <div *ngIf="user" style="padding-left: 50px;">
            <h2>{{user.login}}</h2>
            <p>ID: {{user.id}}</p>
            <p>Login: {{user.login}}</p>
        </div>
    `
})
export class UserDetailComponent {
    user: User;

    constructor(
        private userService: UserService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.route.params.forEach((params: Params) => {
            let id = params['id'];
            this.userService.getUser(id).then(user => this.user = user);
        });
    }

    gotoList(): void {
        let link = ['/user-list'];
        this.router.navigate(link);
    }
}