import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
        {{title}}, study app.
        <br>
        <router-outlet></router-outlet>
    `
})
export class AppComponent {
    title = 'Hello';
}