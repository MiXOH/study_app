import { Component }                from '@angular/core';
import { Router }                   from '@angular/router';

import { User }                     from '../../models/user';
import { UserService }              from '../../services/user.service';


@Component({
    selector: 'user-list',
    template: `
        <h4>User list:</h4>
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Login</th>
                    <th>Last name</th>
                    <th>Firts name</th>
                    <th>Middle name</th>
                    <th></th>
                </tr>
            </thead>
            <tr *ngFor="let user of userList">
                <td>{{user.id}}</td>
                <td>{{user.login}}</td>
                <td>{{user.last_name}}</td>
                <td>{{user.first_name}}</td>
                <td>{{user.middle_name}}</td>
                <td><a (click)="gotoDetail(user)">detail >></a></td>
            </tr>
        </table>
    `
})
export class UserListComponent {
    public userList: User[];

    constructor (
        private userService: UserService,
        private router: Router
    ) {
        this.getUsers();
    }

    getUsers(): void {
        this.userService.getUsers().then(users => this.userList = users);

    }

    gotoDetail(user: User): void {
        let link = ['/user-detail', user.id];
        this.router.navigate(link);
    }
}