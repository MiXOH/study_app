import { Injectable }           from '@angular/core';

import '../rxjs-extensions';

import { User }                 from "../models/user";

@Injectable()
export class UserService {
    mockUsers: User[] = [
        {
            id: 1,
            login: 'user1',
            first_name: 'user1',
            middle_name: 'user1',
            last_name: 'user1'
        },
        {
            id: 2,
            login: 'user2',
            first_name: 'user2',
            middle_name: 'user2',
            last_name: 'user2'
        },
        {
            id: 3,
            login: 'user3',
            first_name: 'user3',
            middle_name: 'user3',
            last_name: 'user3'
        },
        {
            id: 4,
            login: 'user4',
            first_name: 'user4',
            middle_name: 'user4',
            last_name: 'user4'
        },
        {
            id: 5,
            login: 'user5',
            first_name: 'user5',
            middle_name: 'user5',
            last_name: 'user5'
        }
    ];
    mockUser: User = {
        id: 2,
        login: 'user2',
        first_name: 'user2',
        middle_name: 'user2',
        last_name: 'user2'
    };

    getUsers(): Promise<User[]> {
        return Promise.resolve(this.mockUsers);
    }

    getUser(id: number): Promise<User> {
        return Promise.resolve(this.mockUser);
    }

}