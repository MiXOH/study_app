import { NgModule }                     from '@angular/core';
import { BrowserModule }                from '@angular/platform-browser';
import { FormsModule }                  from '@angular/forms';

import { AppComponent }                 from './components/app/app.component';
import { UserDetailComponent }          from "./components/user-detail/user-detail.component";
import { UserListComponent }            from "./components/user-list/user-list.component";
import { UserService }                  from "./services/user.service";
import { routing }                      from "./app.routing";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        routing
    ],
    declarations: [
        AppComponent,
        UserDetailComponent,
        UserListComponent,
    ],
    providers: [
        UserService
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule {
}
