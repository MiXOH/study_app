import { ModuleWithProviders }                  from '@angular/core';
import { Routes, RouterModule }                 from '@angular/router';

import { UserDetailComponent }                  from './components/user-detail/user-detail.component';
import { UserListComponent }                    from './components/user-list/user-list.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'user-list',
        pathMatch: 'full'
    },
    {
        path: 'user-detail/:id',
        component: UserDetailComponent
    },
    {
        path: 'user-list',
        component: UserListComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);