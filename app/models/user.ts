export class User {
    public id: number;
    public login: string;
    public first_name: string;
    public middle_name: string;
    public last_name: string;
}